# Cordova Appstorage Plugin

Allows you to use the app storage on iOS devices, which remains until the app is uninstalled.
