package com.james24.plugin;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;

public class Appstorage extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {

        if (action.equals("testload")) {

            String name = data.getString(0);
            String message = "Load " + name;
            callbackContext.success(message);

            return true;

        } else {
            
            return false;

        }
    }
}
