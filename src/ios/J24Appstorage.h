#import <Cordova/CDV.h>

@interface J24Appstorage : CDVPlugin

- (void) get:(CDVInvokedUrlCommand*)command;
- (void) set:(CDVInvokedUrlCommand*)command;

@end