#import "J24Appstorage.h"
#import "AppDelegate.h"

@implementation J24Appstorage

- (void)get:(CDVInvokedUrlCommand*)command
{
    NSString* callbackId = [command callbackId];
    NSString* varName = [[command arguments] objectAtIndex:0];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* varVal = [defaults objectForKey:varName];
    NSLog(@"get defaults[%@] = %@", varName, varVal);

    NSString* msg = varVal;
    CDVPluginResult* result = [CDVPluginResult
                               resultWithStatus:CDVCommandStatus_OK
                               messageAsString:msg];

    [self success:result callbackId:callbackId];
}

- (void)set:(CDVInvokedUrlCommand*)command
{
    NSString* callbackId = [command callbackId];
    NSString* varName = [[command arguments] objectAtIndex:0];
    NSString* varVal = [[command arguments] objectAtIndex:1];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:varVal forKey:varName];
    NSLog(@"set defaults[%@] = %@", varName, varVal);

    NSString* msg = @"";
    CDVPluginResult* result = [CDVPluginResult
                               resultWithStatus:CDVCommandStatus_OK
                               messageAsString:msg];

    [self success:result callbackId:callbackId];
}

@end