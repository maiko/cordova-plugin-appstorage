/*global cordova, module*/

module.exports = {
    get: function (name, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Appstorage", "get", [name]);
    },
    set: function (name, value, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Appstorage", "set", [name, value]);
    }
};
